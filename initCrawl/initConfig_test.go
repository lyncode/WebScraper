package initCrawl

import (
	"log"
	"testing"
	"wikipediaCrawler/mongoDriver"
)

func TestReadConfig(t *testing.T) {
	ReadConfig()
	// check the function ReadConfig can read the configuration file right or not
	if CrawlerConfig.MongoAddr != "mongodb://mongo:27017" {
		t.Fatalf("test fail")
	}
	// test for connection
	client := mongoDriver.Connect("mongodb://localhost:27017")
	if client == nil {
		log.Fatal("connect to db fail")
	}
}
