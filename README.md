# WikiPedia Scraper

## this is a sub-project of one [micro-service project](https://gitlab.com/lyncode/devops)

this project is a scraper which collect the first paragraph of every wikipedia page and store it into a replicated mongoDB

## Architecture

   wikipedia -----> scraper -----> database