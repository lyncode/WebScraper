package wiki

import (
	"fmt"
	"testing"
)

func TestWikiDoc(t *testing.T) {
	contentPool := make(chan string, 100)
	urlPool := make(chan string, 1000)
	url := "https://en.wikipedia.org/wiki/Istiodactylus"
	url, content := WikiDoc(url, contentPool, urlPool)
	fmt.Println(content)

}

func TestWikiMainPage(t *testing.T) {
	mainPageContent := WikiMainPage()
	inThisDay, _ := mainPageContent["inThisDay"]
	oldTheDay, _ := mainPageContent["oldTheDay"]
	didYouKnow, _ := mainPageContent["didYouKnow"]
	fmt.Println(inThisDay, oldTheDay, didYouKnow)

}
