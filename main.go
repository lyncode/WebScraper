package main

import (
	"fmt"
	"wikipediaCrawler/initCrawl"
	"wikipediaCrawler/mongoDriver"
	"wikipediaCrawler/wiki"
)

func main() {
	initCrawl.ReadConfig()
	client := mongoDriver.Connect(initCrawl.CrawlerConfig.MongoAddr)
	defer mongoDriver.Disconnect(client)
	mainPageColl := client.Database("wikipedia").Collection("MainPage")
	DocsColl := client.Database("wikipedia").Collection("Docs")

	fmt.Println("wikipedia scraper begins...")
	mainPageChan := make(chan wiki.TodayMainPage, 10)
	docChan := make(chan wiki.WikiDocParagraph, 10)
	urlPool := make(chan string, 10000) // set to 10000 to cater too many link in one page
	contentPool := make(chan string, 100)

	// set 2 independent goroutine to fetch main page content and the docs content

	go wiki.FetchMainPage(mainPageChan)
	entry := "https://en.wikipedia.org/wiki/watch"
	go wiki.FetchDocs(contentPool, urlPool, entry, docChan)

	// infinite loop to read content in 2 content channel and insert them into database
	wiki.InsertDB(mainPageChan, docChan, mainPageColl, DocsColl)

}
